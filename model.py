from __future__ import print_function
from collections import Counter
from nltk.tokenize import sent_tokenize, word_tokenize
import nltk, numpy as np
import math
from nltk.corpus import state_union
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
import pandas as pd
from rouge_score import rouge_scorer
import sys
import pickle
import os
import pandas as pd
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
import spacy
from spacy import displacy
from collections import Counter
import en_core_web_sm
nlp = en_core_web_sm.load()

############ PHASE 1st ################

#JACCARD SIMILARITY

def jaccard_similarity(list1, list2):
    s1 = set(list1)
    s2 = set(list2)
    return len(s1.intersection(s2)) / len(s1.union(s2))

#TF-ISF
def TFISF(i):
      s, a, su = [], [], 0
      s = np.unique(m[i])
      total1 = len(m[i])
      total2 = len(tokens)
      for k in range(0,len(s)):
               cc = s[k]
               count = m[i].count(cc)
               a.append(float(count/total1))  
      for i in range(0,len(s)):
              counter = 1
              for j in range(0,total2):
                       if s[i] in tokens[j]:
                           counter+=1  
              z = float(total2/counter)                  
              su = su + a[i]*(math.log(z,10))
      return su


main = 'input/BBC News Summary/News Articles/tech/'


score_1 = []
score_L = []
mat = []
tk = []
temp1 = []
x = os.listdir(main)
x.sort()

for i in x:
  doc = []
  path = main+str(i)

  stop_words = set(stopwords.words("english"))
  file_content = open(path).read()
  para = file_content.split("\n")
  punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''  
  ps = PorterStemmer()
  tokens = nltk.sent_tokenize(file_content)
  tk.append(tokens)
  m, n, xx, SFM, temp = len(tokens)*[25*[0]], len(tokens)*[25*[0]], len(tokens)*[25*[0]], [[0.0 for i in range(9)] for j in range(len(tokens))], [[0.0 for i in range(9)] for j in range(len(tokens))]
  NE = []
  COS = []
  SCORE_SENT = []
  INDEX = []
  SUMMARY = []
  JACCARD = []
  INDEX_SUMM = []

  #total paragraphs
  p = len(para) - 1


  #THEMATIC_PRE_REQUISITE
  doc = file_content.split()
  out = map(lambda x:x.lower(), doc)
  doc = list(out)
  k = -1
  for i in doc:
    k+=1
    if i in stop_words:
        doc.pop(k)
    if i in punctuations:
        doc.pop(k)
    if k<len(doc):    
      doc[k] = ps.stem(doc[k])
  counter = Counter(doc)
  most_occur = counter.most_common(10)  


  #NAMED_ENTITIES

  for i in range(0,len(tokens)):
          m[i] = nltk.word_tokenize(tokens[i])
          xx[i] = nltk.word_tokenize(tokens[i])
          #NAMED_ENTITIES
          doc = nlp(tokens[i])
          NE.append(len(doc.ents))

  
  #POS_TAGGING
  for i in range(0,len(tokens)):
            n[i] = nltk.pos_tag(m[i])
         
  #STEMMING
  for i in range(0,len(tokens)):
      for j in range(0,len(m[i])):
          s = m[i][j]
          v = ps.stem(s)
          m[i][j] = v


  #STOP_WORDS
  k = -1
  for i in range(0,len(tokens)):
      for j in m[i]:
          k+=1
          if j in stop_words:
                  m[i].pop(k)
      k = -1



  k = -1
  for i in range(0,len(tokens)):
      for j in xx[i]:
          k+=1
          if j in stop_words:
                  xx[i].pop(k)
      k = -1

  #PUNCTUATIONS
  k = -1
  for i in range(0,len(tokens)):
    for j in m[i]:
         k+=1
         if j in punctuations:
                m[i].pop(k)
    k = -1

  k = -1
  for i in range(0,len(tokens)):
      for j in xx[i]:
          k+=1
          if j in punctuations:
                  xx[i].pop(k)
      k = -1



  #TF-ISF
  SCORE = []
  for i in range(0,len(tokens)):
              value = TFISF(i)
              SCORE.append(value)



  #COSINE VALUE
  MAX = SCORE.index(max(SCORE))
  for i in range(0,len(tokens)):
      hh, vv = set(m[i]), set(m[MAX])
      r_vector = hh.union(vv)
      l1, l2 = [], []  
      for w in r_vector:
              if w in m[i]: l1.append(1)  
              else: l1.append(0)
              if w in m[MAX]: l2.append(1)
              else: l2.append(0)
      c = 0
      for i in range(len(r_vector)):
          c+= l1[i]*l2[i]
      cosine = c / float((sum(l1)*sum(l2))**0.5)      
      COS.append(cosine)

  #THEMATIC
  thematic = []
  for i in range(0,len(tokens)):
        counter= 0
        for j in range(0,10):
                if most_occur[j][0] in m[i]: counter+=1
        f = float(counter/len(m[i]))
        thematic.append(f)



  #SENTENCE_POSITION
  sent_pos = []
  for i in range(0,len(tokens)):
          if i == 0 or i == (len(tokens) - 1):
                        sent_pos.append(1)
          else:
                        mn = 0.2*len(tokens)
                        mx = 0.4*len(tokens)

                        x = math.cos((i-mn)/(1/(mx) - mn))
                        sent_pos.append(x)



  #SENTENCE_LENGTH
  sent_len = []
  for i in range(0,len(tokens)):
          if len(m[i]) < 3 :
                        sent_len.append(0)
          else :
                        sent_len.append(len(m[i]))


  #SENT_POS_PARA
  sent_pos_para = []
  for j in range(0,p):
      for k in range(0,len(nltk.sent_tokenize(para[j]))):
            if k == 0 or k == ( len(nltk.sent_tokenize(para[j])) -1 ) :
                    sent_pos_para.append(1)
            else :                  
                    sent_pos_para.append(0)


  #NO_OF_PN
  NOP = []
  for i in range(0,len(tokens)):
        counter = 0
        for j in range(0,len(n[i])):
                if n[i][j][1] == 'NNP' :
                  counter+=1
        NOP.append(counter)


  #NO_OF_NUMERAL
  NONU = []
  for i in range(0,len(tokens)):
        counter = 0
        for j in range(0,len(xx[i])):
                if xx[i][j].isdigit() :
                  counter+=1
        NONU.append(counter)


  temp1.append(xx)
  
  #CREATING SENTENCE FEATURE MATRIX
  for i in range(0,len(tokens)):
            SFM[i][0] = round(thematic[i],2)


  for i in range(0,len(tokens)):
            SFM[i][1] = round(sent_pos[i],2)


  for i in range(0,len(tokens)):
            SFM[i][2] = round(sent_len[i],2)


  for i in range(0,len(tokens)):
            SFM[i][3] = round(sent_pos_para[i],2)


  for i in range(0,len(tokens)):
            SFM[i][4] = round(NOP[i],2)


  for i in range(0,len(tokens)):
            SFM[i][5] = round(NONU[i],2)


  for i in range(0,len(tokens)):
            SFM[i][6] = round(NE[i],2)


  for i in range(0,len(tokens)):
            SFM[i][7] = round(SCORE[i],2)


  for i in range(0,len(tokens)):
            SFM[i][8] = round(COS[i],2)

  columns = ['Thematic Words Count','Sentence Position','Sentence Lenght','Sentence Position Paragraph','Pronouns Count','Nouns Count','Numeral Count','TF-ISF','Cosine Similarity']
  SEM = pd.DataFrame(SFM,columns = columns)
  #print(SEM.head(2))
  SEM = SEM.astype('float32')
  #SEM.head(2)

  SFM = np.array(SFM)
  mat.append(SFM)


############ PHASE 2nd ################

class RBM():

    def __init__(self, hid , vis,input):
        self.hid = hid
        self.vis = vis
        self.input = input
        self.w = np.asarray(np.random.RandomState(300).uniform(np.sqrt(2 +  7. / (hid + vis)),
                                                               np.sqrt(4 + 6. /(hid + vis))
                ,size=(hid,input)))
        

    def sig_m(self, n):
        return 1.0 / (1 + np.exp(-n))

    def Train(self,epoch,lr,Data):

        n = Data.shape[0]
  
        for e in range(epoch):

          act = np.dot(Data, self.w)
          act_p = self.sig_m(act)
          act_p[:,0] = 1
            
          hid_s = act_p > np.random.rand(act.shape[1],n)
          pos_a = np.dot(Data.T, act_p)


          act_n = np.dot(pos_a, self.w.T)
          act_n = self.sig_m(act_n)
          act_n[:,0] = 1

          hid_n = np.dot(act_n, self.w)
          hid_n = self.sig_m(hid_n)
          n_a = np.dot(act_n.T, hid_n)

          self.w = self.w + lr * ((pos_a - n_a / n))

          return self.w

models = []
for i in range(len(mat)):
  model = RBM(9,9,mat[i].shape[0])
  w = model.Train(100,0.01,mat[i])
  models.append(w.T)

############ PHASE 3rd ################

main = 'input/BBC News Summary/Summaries/tech/'

x = os.listdir(main)
x.sort()

sum_len =[]
for i in x:
  path = main+i
  f = open(path, "r")
  st = f.read()
  tok = st.split('.')
  sum_len.append(len(tok))


finalsum = []

for i in range(len(mat)):
  SCORE_SENT = []
  INDEX = []
  SUMMARY = []
  JACCARD = []

  #SCORE GENERATION
  for j in range(len(tk[i])):
      xs = sum(models[i][j])
      SCORE_SENT.append(xs )
      INDEX.append(j)

  n = len(SCORE_SENT)

  #SORTING THE INDICES OF SENTENCES OF ACCORDING TO SCORE
  for t in range(n-1):
      for l in range(0,(n-t-1)):
          if SCORE_SENT[l] < SCORE_SENT[l+1]:
              SCORE_SENT[l], SCORE_SENT[l+1] = SCORE_SENT[l+1], SCORE_SENT[l]
              INDEX[l], INDEX[l+1] = INDEX[l+1], INDEX[l]  

  #FINDING THE JACCARD SIMILARITY VALUES FOR 1st HALF TOP SENTENCE SCORERS
  for t in range(int(len(INDEX)/2)):
      JACCARD.append(jaccard_similarity(temp1[i][INDEX[0]], temp1[i][INDEX[t+1]]))
      
  #SORTING INDICES OF SENTENCES ACCORDING TO JACCARD SIMILARITY VALUE
  for t in range(len(JACCARD)-1):
      for l in range((len(JACCARD)-t-1)):
          if JACCARD[l] < JACCARD[l+1]:
             JACCARD[l], JACCARD[l+1] = JACCARD[l+1], JACCARD[l]
             INDEX[l+1], INDEX[l+2] = INDEX[l+2], INDEX[l+1]

  #FINALLY, SORTING THE INDICES OF SENTENCES IN ASCENDING ORDER FOR SELECTION FROM DOCUMENT
  su_len = sum_len[i]

  for t in range(su_len):
      for l in range(su_len-t-1):
          if INDEX[l] > INDEX[l+1]:    
                 INDEX[l], INDEX[l+1] = INDEX[l+1], INDEX[l]

  #SELECTION OF SENTENCES
  for t in range(su_len):
      SUMMARY.append(tk[i][INDEX[t]])

  finalsum.append(SUMMARY)

# Rouge Score

score_1 = []
score_L = []

main = 'input/BBC News Summary/Summaries/tech/'
x = os.listdir(main)
x.sort()

cou = 0
for i in x:
  path = main+i
  f = open(path, "r")
  t = f.read()
  sum = ''
  ttt = finalsum[cou]

  for ii in ttt:
    sum = sum + ii 
  sum = sum.replace('\n','')
  sum = sum.replace('"','')

  sc = rouge_scorer.RougeScorer(['rouge1', 'rougeL'], use_stemmer=True)
  ss = sc.score(sum,t)
    
  #print(ss)
  
  score_1.append(list(ss['rouge1']))
  score_L.append(list(ss['rougeL']))

  cou = cou + 1

col = ['Precision','Recall','FMeasure']

score_1d = pd.DataFrame(score_1,columns = col)
score_Ld = pd.DataFrame(score_L,columns = col)


for i in range(len(finalsum)-390):
  print('Document : '+str(i+1))
  org1 = tk[i]
  s1 = ''
  for jj in org1:
    s1 = s1 + jj 
  s1 = s1.replace('\n','')
  s1 = s1.replace('"','')
  print(s1,'\n')

  print('Model Generated Summary : '+str(i+1))
  org2 = finalsum[i]
  s2 = ''
  for kk in org2:
    s2 = s2 + kk 
  s2 = s2.replace('\n','')
  s2 = s2.replace('"','')
  print(s2,'\n')